#!/usr/bin/env python
from SimpleHTTPServer import SimpleHTTPRequestHandler 
import SocketServer
import cgi
from jinja2 import Template
from time import strftime, mktime, strptime
import os
from datetime import datetime


DB_FILE = 'db.txt'
RESOURCES = ['index.html', 'success.html', DB_FILE]


class MyRequestHandler(SimpleHTTPRequestHandler):
    def load_tpl(self, tpl):
        try:
            with open(tpl, 'r') as f:
                return f.read(); 
        except IOError:
            return self.render_to_response('500.html', status_code=500)
        
    def render_to_response(self, tpl, context={}, status_code=200):
        t = self.load_tpl(tpl)
        html = Template(t).render(context)
        return self._send_response(status_code, html)

    def _send_response(self, status_code, html=''):
        self.send_response(status_code)
        self.end_headers()
        return self.wfile.write(html)

    def do_GET(self):
        if self.path == '/':
            self.path = '/index.html'
        if not self.path[1:] in RESOURCES:
            return self.render_to_response('404.html', status_code=404)
        if self.path[1:] == DB_FILE:
            return self.serve_file()
        return self.render_to_response(self.path[1:])

    def redirect(self, url):
        self.send_response(301)
        self.send_header('Location', url)
        self.end_headers()
    
    def modified_since_request(self):
        """ 
        Check if it is needed to serve the file again or just 
        send a 304 Not Modified response
        """
        file_modified_time = os.path.getmtime(DB_FILE)
        if 'If-Modified-Since' in self.headers:
            modified_since_req = mktime(strptime(self.headers['If-Modified-Since'], 
                                                 "%a, %d %b %Y %H:%M:%S GMT"))  
            return file_modified_time > modified_since_req
        return True

    def serve_file(self):

        if not self.modified_since_request():
            self.send_response(304)
            self.end_headers()
            return

        with open(DB_FILE, 'r') as f:
            self.send_response(200)
            self.send_header('Content_type', 'text/plain')
            self.send_header('Last-Modified', 
                             strftime("%a, %d %b %Y %H:%M:%S GMT",
                                      datetime.fromtimestamp(
                                          os.path.getmtime(DB_FILE)).timetuple()
                                     )
                            )
            self.send_header('Content-Disposition',
                               'attachment; filename={0}'.format(DB_FILE))
            self.end_headers()
            return self.wfile.write(f.read())

    def save_file(self, data):
        t = self.load_tpl('save_file.txt')
        txt = Template(t).render(data)

        with open(DB_FILE, 'a') as db:
            db.write(txt)

    def do_POST(self):
        if self.path=="/index.html":
            form = cgi.FieldStorage(
                fp=self.rfile, 
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE':self.headers['Content-Type'],
            })
 
            if not all([field in form for field in ('your_name', 
                                                    'company', 
                                                    'favorite_color')]):
                return self.render_to_response('index.html', {
                    "error": "Todos los campos son requeridos",
                    "fields": form
                })
            
            data = {
                'date': strftime('%H:%M:%S %Y/%m/%d'),
                'your_name': form['your_name'].value,
                'favorite_color': form['favorite_color'].value,
                'company': form['company'].value,
            }
            self.save_file(data)
            self.redirect('success.html')


if __name__ == '__main__':
    port = 8080
    SocketServer.TCPServer.allow_reuse_address = True
    server = SocketServer.TCPServer(('127.0.0.1', port), MyRequestHandler)
    print "serving at http://{0}:{1}".format('127.0.0.1', port)
    server.serve_forever()
